/*
 * Nagodigo - PSPICE to SPICE3 / PROSPICE model converter
 * Copyright (c) 2021.  Vladimir Garistov vl.garistov@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//! Parameter replacement and expression evaluation

use std::fmt::{Display, Formatter};

/// Name-value pair for a single parameter
#[derive(Debug, Clone, PartialEq)]
pub struct Param
{
	pub name: String,
	pub value: String
}

// TODO: Detect circular definitions; Evaluate expressions to constant values
impl Param
{
	/// Constructs a new [`Param`] from name and value. Validates the name and value.
	pub fn new(name: &str, value: &str) -> Option<Param>
	{
		// TODO: Check if an underscore can be used in parameter names
		if !name.chars().all(|c| c.is_ascii_alphanumeric() || c == '_') ||
			match name.chars().next()
			{
				Some(c) => c.is_numeric(),
				None => true
			} || !value.chars().all(|c| char_usable_in_param_value(c)) {return None;}
		Some(Param
		{
			name: String::from(name),
			value: String::from(value).replace("{", "(").replace("}", ")")
		})
	}

	/// Parses a .PARAM directive to generate a vector of [`Param`]s.
	///
	/// # Example
	/// ```ignore
	/// let p = Param::from_str(".PARAM BETA=250\n").unwrap();
	/// assert_eq!(p, vec![Param{name: String::from("BETA"), value: String::from("250")}]);
	/// ```
	pub fn from_str(input: &str) -> Option<Vec<Param>>
	{
		let input_split = input.split_ascii_whitespace().collect::<Vec<&str>>();
		if input_split.len() < 2 {return None;}
		if !input_split[0].to_uppercase().eq(".PARAM") {return None;}
		let input_no_directive = input_split[1..].join(" ");
		let mut input_split = input_no_directive.split(',');
		let mut params: Vec<Param> = Vec::new();
		for name_value_pair in input_split
		{
			let split_name_value = name_value_pair.split("=").collect::<Vec<&str>>();
			if split_name_value.len() != 2 {return None;}
			if let Some(new_param) = Param::new(split_name_value[0].trim(), split_name_value[1].trim())
			{
				params.push(new_param);
			}
		}
		if params.len() > 0	{Some(params)} else {None}
	}

	/// Parses the inline parameters of a component to generate a vector of [`Param`]s.
	/// input must contain only the parameter list, not the entire SPICE component line.
	pub fn from_str_inline(input: &str) -> Option<Vec<Param>>
	{
		let mut params: Vec<Param> = Vec::new();
		let mut input_split = input.split_ascii_whitespace();
		for name_value_pair in input_split
		{
			let split_name_value = name_value_pair.split("=").collect::<Vec<&str>>();
			if split_name_value.len() != 2 {return None;}
			if let Some(new_param) = Param::new(split_name_value[0].trim(), split_name_value[1].trim())
			{
				params.push(new_param);
			}
		}
		if params.len() > 0	{Some(params)} else {None}
	}
}

// TODO: find the actual requirements to parameter names and values in PSPICE documentation
/// Validates a character for use in parameter value
fn char_usable_in_param_value(c: char) -> bool
{
	c.is_ascii_alphanumeric() ||
		c == ' ' || c == '\t' || c == '+' || c == '-' || c == '/' || c == '*' ||
		c == '(' || c == ')' || c == '.' || c == '^' || c == '{' || c == '}'
}

#[cfg(test)]
mod tests
{
	use crate::param::Param;

	#[test]
	fn single_parameter()
	{
		assert_eq!(Param::from_str(".PARAM KUR=16cm").unwrap(),
		           vec![Param{name: String::from("KUR"), value: String::from("16cm")}]);
	}

	#[test]
	fn single_parameter_with_whitespace()
	{
		assert_eq!(Param::from_str(".PARAM  \t  KUR=16cm \n").unwrap(),
		           vec![Param{name: String::from("KUR"), value: String::from("16cm")}]);
	}

	#[test]
	fn multiple_parameters()
	{
		assert_eq!(Param::from_str(".PARAM KUR=16cm, Kilata=100, TWO_PI = {2*3.14159}, BANDWIDTH={100kHz/3} , VEE = -12V ,VSUPPLY = 5V").unwrap(),
		           vec![Param{name: String::from("KUR"), value: String::from("16cm")},
		                Param{name: String::from("Kilata"), value: String::from("100")},
		                Param{name: String::from("TWO_PI"), value: String::from("(2*3.14159)")},
		                Param{name: String::from("BANDWIDTH"), value: String::from("(100kHz/3)")},
		                Param{name: String::from("VEE"), value: String::from("-12V")},
		                Param{name: String::from("VSUPPLY"), value: String::from("5V")}]);
	}
}