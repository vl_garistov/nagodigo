/*
 * Nagodigo - PSPICE to SPICE3 / PROSPICE model converter
 * Copyright (c) 2021.  Vladimir Garistov vl.garistov@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//! Module for parsing SPICE files

use std::iter::Peekable;
use std::str::Split;

/// An iterator over SPICE components and directives. Joins component declarations and directives split across multiple lines.
/// Converts inline comments to full-line comments.
#[allow(non_camel_case_types)]
pub struct SPICE_Iterator<'a>
{
	input_lines: Peekable<Split<'a, char>>,
	line_counter: u32,
	buffer: String
}

impl SPICE_Iterator<'_>
{
	/// Returns the number of input lines parsed so far. Note that this is not the same as the number of parsed components/directives because some
	/// components are spread across multiple lines and empty lines do not provide any components/directives but are still counted as lines.
	pub fn get_line_count(&self) -> u32
	{
		self.line_counter
	}
}

impl<'a> Iterator for SPICE_Iterator<'a>
{
	type Item = String;

	fn next(&mut self) -> Option<Self::Item>
	{
		if self.buffer.len() > 0 && match self.input_lines.peek()
		{
			None => true,
			Some(next_line) =>
			{
				match next_line.chars().next()
				{
					None => false,
					Some('+' | ' ' | '\t' | '*' | ';') => false,
					Some(_) => true,
				}
			}
		}
		{
			let spice_cmd = self.buffer.clone();
			self.buffer.clear();
			return Some(spice_cmd);
		}
		while let Some(line) = self.input_lines.next()
		{
			self.line_counter += 1;
			match line.chars().next()
			{
				None => break,
				Some(' ' | '\t' | '*' | ';') => return Some(String::from(line)),
				Some(_) => ()
			}

			if line.contains(';')
			{
				let mut split_line = line.split(';');
				let spice_cmd = split_line.next().unwrap_or("");
				if let Some('+') = spice_cmd.chars().next()
				{
					self.buffer.push_str(&spice_cmd[1..]);
				}
				else
				{
					self.buffer.push_str(spice_cmd);
				}
				let mut comment = String::from(';');
				comment.push_str(split_line.next().unwrap_or(""));
				return Some(comment);
			}
			if let Some('+') = line.chars().next()
			{
				self.buffer.push_str(&line[1..]);
			}
			else
			{
				self.buffer.push_str(line);
			}
			if match self.input_lines.peek()
			{
				None => true,
				Some(next_line) =>
				{
					if let Some('+' | ' ' | '\t' | '*' | ';') = next_line.chars().next() {false} else {true}
				}
			}
			{
				let spice_cmd = self.buffer.clone();
				self.buffer.clear();
				return Some(spice_cmd);
			}
		}
		None
	}
}

/// A trait that allows a [`SPICE_Iterator`] to be constructed from the data type that implements it.
#[allow(non_camel_case_types)]
pub trait SPICE_Iter
{
	/// Create a [`SPICE_Iterator`]
	///
	/// # Example
	/// ```ignore
	/// let netlist = String::from("R1 3 1 10k\nC1 3 2 100n ; A comment\nC2 1 0 270n\n").spice_lines();
	/// assert_eq!(netlist.collect(), vec![
	/// 	String::from("R1 3 1 10k"),
	///     String::from("; A comment"),
	///     String::from("C1 3 2 100n "),
	///     String::from("C2 1 0 270n")
	/// ]);
	/// ```
	fn spice_lines(&self) -> SPICE_Iterator;
}

impl SPICE_Iter for String
{
	fn spice_lines(&self) -> SPICE_Iterator
	{
		SPICE_Iterator
		{
			input_lines: self.split('\n').peekable(),
			line_counter: 0,
			buffer: String::new()
		}
	}
}

#[cfg(test)]
mod tests
{
	use super::SPICE_Iter;

	#[test]
	fn single_line()
	{
		assert_eq!(String::from("R1 1 0 10k").spice_lines().next().unwrap(),
		           String::from("R1 1 0 10k"));
	}

	#[test]
	fn triple_line()
	{
		assert_eq!(String::from("B1 2 3 Value={A+B+C-\n+ C+D*7+\n+(-3)/25}").spice_lines().next().unwrap(),
		           String::from("B1 2 3 Value={A+B+C- C+D*7+(-3)/25}"));
	}

	#[test]
	fn triple_line_from_many()
	{
		let test_string = String::from("B1 2 3 Value={A+B+C-\n+ C+D*7+\n+(-3)/25}\nR1 1 0 10k\nC1 1 2 100n");
		let mut test_iter = test_string.spice_lines();
		assert_eq!(test_iter.next().unwrap(),
		           String::from("B1 2 3 Value={A+B+C- C+D*7+(-3)/25}"));
		assert_eq!(test_iter.get_line_count(), 3);
	}

	#[test]
	fn multiple_inline_comments()
	{
		let test_string = String::from("B1 2 3 Value={A+B+C- ; Some stupid\n+ C+D*7+; multiline\n+(-3)/25} ;comment\n\
												  R1 1 0 10k ;I need sleep\nC1 1 2 100n");
		assert_eq!(test_string.spice_lines().collect::<Vec<String>>(), vec![
			String::from("; Some stupid"),
			String::from("; multiline"),
			String::from(";comment"),
			String::from("B1 2 3 Value={A+B+C-  C+D*7+(-3)/25} "),
			String::from(";I need sleep"),
			String::from("R1 1 0 10k "),
			String::from("C1 1 2 100n")
		]);
		let mut test_iter = test_string.spice_lines();
		while let Some(_) = test_iter.next(){};
		assert_eq!(test_iter.get_line_count(), 5);
	}

	// TODO: More tests (should not return empty lines but should advance line counter, weird corner cases)
}