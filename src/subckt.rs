/*
 * Nagodigo - PSPICE to SPICE3 / PROSPICE model converter
 * Copyright (c) 2021.  Vladimir Garistov vl.garistov@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//! A subcircuit that includes other components and directives

use crate::param::Param;
use crate::components::{SPICE_Conversion, Component};
use crate::spice_iter::SPICE_Iterator;
use crate::{ConversionError, ErrorInfo};
use std::fmt::Display;
use std::fmt;
use std::error::Error;
use std::rc::Rc;
use std::cell::RefCell;
use std::ops::Add;

// TODO: Add a list of parents to detect circular definitions
// TODO: Add lists of local models. Detect circular AKOs
/// A subcircuit that includes other components and directives. Also contains internal lists of local parameters and nested subcircuits.
pub struct Subcircuit
{
	name: String,
	params: Vec<Param>,
	spice_text: String,
	contents: Vec<Component>,
	local_subcircuits: Vec<Rc<RefCell<Subcircuit>>>,
	converted: bool
}

impl Subcircuit
{
	/// Parses .SUBCKT directive and constructs a new [`Subcircuit`].
	/// Returns an empty [`Subcircuit`], relies on a call to [`add_from_spice_iter()`] after it.
	pub fn from_str(input: &str) -> Option<Subcircuit>
	{
		let input_split = input.split("\"").collect::<Vec<&str>>();
		let mut input_no_text = String::new();
		let mut i = 0;
		while i < input_split.len()
		{
			input_no_text.push_str(input_split[i]);
			i += 2;
		}

		// Note that PARAMS:smth (no interval) is parsed without errors. This probably isn't valid PSPICE syntax but it could be.
		let input_split = input_no_text.split(&[' ', '='][..]).collect::<Vec<&str>>();
		if input_split.len() < 2 {return None;}
		if !input_split[0].to_uppercase().eq(".SUBCKT") {return None;}
		let name = String::from(input_split[1]);
		let mut params: Vec<Param> = Vec::new();
		let mut i = 2;
		let mut name_ref: &str = &"";
		let mut value_ref: &str;
		let mut name_found = false;
		while i < input_split.len()
		{
			if input_split[i].to_uppercase().contains("PARAMS:")
			{
				let param_statement_split = input_split[i].split(":").collect::<Vec<&str>>();
				if param_statement_split.len() > 2 {return None;}
				if param_statement_split.len() == 2 && !param_statement_split[1].eq("")
				{
					name_ref = param_statement_split[1].trim();
					name_found = true;
				}
				i += 1;
				while i < input_split.len()
				{
					if !input_split[i].eq("")
					{
						if name_found
						{
							value_ref = input_split[i].trim();
							name_found = false;
							if let Some(new_param) = Param::new(name_ref, value_ref)
							{
								params.push(new_param);
							}
							else
							{
								return None;
							}
						}
						else
						{
							name_ref = input_split[i].trim();
							name_found = true;
						}
					}
					i += 1;
				}
				return Some(Subcircuit
				{
					name,
					params,
					spice_text: String::from(input),
					contents: Vec::new(),
					local_subcircuits: Vec::new(),
					converted: false
				})
			}
			i += 1;
		}
		None
	}

	// TODO
	/// Parses a subcircuit reference and constructs a new [`Subcircuit`].
	/// Returns an empty [`Subcircuit`], relies on the caller to add components.
	/// Used for creating copies of subcircuits referenced with explicit parameter values.
	pub fn from_reference(input: &str) -> Option<Subcircuit>
	{
		None
	}

	/// Adds a new component to the subcircuit.
	fn add_component(&mut self, component: Component)
	{
		self.contents.push(component);
	}

	/// Takes a [`Spice_Iterator`] and adds the components parsed by the iterator up to and including an .END directive.
	pub fn add_from_spice_iter(&mut self, spice_input: &mut SPICE_Iterator, error_info: &mut ErrorInfo)
		-> Result<(), Box<dyn Error>>
	{
		while let Some(spice_line) = spice_input.next()
		{
			if let Some(new_component) = Component::from_str(&spice_line)
			{
				if let Component::UnsupportedComponent(_) = new_component
				{
					if error_info.force
					{
						*error_info.errors_ignored = true;
						eprintln!("ERROR while parsing {}: {}", error_info.filename, ConversionError::Unsupported(spice_input.get_line_count()));
						continue;
					}
					else
					{
						return Err(Box::new(ConversionError::Unsupported(spice_input.get_line_count())));
					}
				}

				let last = if let Component::End(_) = new_component {true} else {false};
				self.contents.push(match new_component
				{
					Component::SubcircuitDefinition(sub) =>
					{
						self.local_subcircuits.push(sub.clone());
						sub.borrow_mut().add_from_spice_iter(spice_input, error_info)?;
						Component::SubcircuitDefinition(sub)
					},
					comp=> comp
				});
				if last {return Ok(())};
			}
			else if let Some(mut new_params) = Param::from_str(&spice_line)
			{
				self.params.append(&mut new_params);
			}
			else if error_info.force
			{
				*error_info.errors_ignored = true;
				eprintln!("ERROR while parsing {}: {}", error_info.filename, ConversionError::ParsingError(spice_input.get_line_count()));
			}
			else
			{
				return Err(Box::new(ConversionError::ParsingError(spice_input.get_line_count())));
			}
		}
		if error_info.force
		{
			*error_info.errors_ignored = true;
			eprintln!("ERROR while parsing {}: {}", error_info.filename, ConversionError::MissingEndStatement(self.name.clone()));
			Ok(())
		}
		else
		{
			Err(Box::new(ConversionError::MissingEndStatement(self.name.clone())))
		}
	}

	/// Adds a new parameter to the subcircuit.
	fn add_param(&mut self, param: Param)
	{
		self.params.push(param);
	}
}

impl Display for Subcircuit
{
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write!(f, "{}\n", self.spice_text)?;
		for component in self.contents.iter()
		{
			write!(f, "{}\n", component)?;
		}
		Ok(())
	}
}

impl SPICE_Conversion for Subcircuit
{
	fn convert(&mut self, global_subcircuits: &Vec<Rc<RefCell<Subcircuit>>>, error_info: &mut ErrorInfo)
	           -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		if !self.converted
		{
			// Note: the .END statement is not checked for validity, instead it is overwritten.
			// Therefore some invalid subcircuits will be silently fixed.
			if let Some(Component::End(end_string)) = self.contents.iter_mut().last()
			{
				if let Some(end_statement) = end_string.split_ascii_whitespace().next()
				{
					*end_string = String::from(end_statement).add(&self.name);
				}
				else
				{
					return Err(Box::new(ConversionError::UnexpectedError()));
				}
			}
			else
			{
				return Err(Box::new(ConversionError::UnexpectedError()));
			}
			if self.spice_text.to_uppercase().contains(" OPTIONAL:")
			{
				if error_info.force
				{
					*error_info.errors_ignored = true;
					eprintln!("ERROR while converting {}: {}", error_info.filename, ConversionError::SubcircuitOptionalNodes(self.spice_text.clone()));
				}
				else
				{
					return Err(Box::new(ConversionError::SubcircuitOptionalNodes(self.spice_text.clone())));
				}
			}
			if self.spice_text.to_uppercase().contains(" TEXT:")
			{
				if error_info.force
				{
					*error_info.errors_ignored = true;
					eprintln!("ERROR while converting {}: {}", error_info.filename, ConversionError::SubcircuitText(self.spice_text.clone()));
				}
				else
				{
					return Err(Box::new(ConversionError::SubcircuitText(self.spice_text.clone())));
				}
			}
			// Remove parameters, text and optional nodes
			if let Some(spice_text_no_unsupported) = self.spice_text.split_inclusive(':').next()
			{
				if spice_text_no_unsupported.contains(':')
				{
					let spice_text_split = spice_text_no_unsupported.split_inclusive(' ').collect::<Vec<&str>>();
					let spice_text_no_unsupported = spice_text_split[..(spice_text_split.len() - 1)].concat();
					self.spice_text = String::from(spice_text_no_unsupported.trim());
				}
				else
				{
					self.spice_text = String::from(spice_text_no_unsupported.trim());
				}
			}
			else
			{
				return Err(Box::new(ConversionError::UnexpectedError()));
			}

			let mut component_buffer:Vec<Component> = Vec::new();
			for component in self.contents.iter_mut()
			{
				if let Some(mut new_component) = component.convert(global_subcircuits, error_info)?
				{
					// TODO: handle the case when new_component is a subcircuit
					component_buffer.append(&mut new_component);
				}
			}
			self.contents.append(&mut component_buffer);
			self.converted = true;
		}
		Ok(None)
	}

	// TODO
	fn replace_params(&mut self, params: Vec<&Vec<Param>>) -> Result<(), Box<dyn Error>>
	{
		Ok(())
	}
}