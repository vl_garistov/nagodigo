/*
 * Nagodigo - PSPICE to SPICE3 / PROSPICE model converter
 * Copyright (c) 2021.  Vladimir Garistov vl.garistov@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

use nagodigo::Config;
use clap::{Arg, App};
use std::process;

fn main()
{
	// TODO: Add logs
	// Declare and parse command-line arguments
	let arg_matches = App::new("Nagodigo")
		.version("0.2.0")
		.author("Vladimir Garistov <vl.garistov@gmail.com>")
		.about("Convert PSPICE models to SPICE3/PROSPICE")
		.arg(Arg::with_name("output filename")
			.short("o")
			.long("output-filename")
			.value_name("FILE")
			.help("Specifies the name of the output file to save the conversion result. Can be specified multiple times when converting multiple \
			          files. Output filenames correspond to input files in the order in which they are specified.")
			.multiple(true)
			.takes_value(true)
			.required(false))
		.arg(Arg::with_name("force")
			.short("f")
			.long("force")
			.help("Forces conversion for files that contain mistakes and/or unsupported components and directives.")
			.takes_value(false)
			.required(false))
		.arg(Arg::with_name("stop_on_error")
			.short("e")
			.long("stop-on-error")
			.help("Stops the conversion whenever an error is encountered, even if the remaining files are correctly formatted.")
			.takes_value(false)
			.required(false)
			.conflicts_with("force"))
		.arg(Arg::with_name("split")
			.short("s")
			.long("split")
			.help("Splits long lines before writing them to the output file.")
			.takes_value(false)
			.required(false))
		.arg(Arg::with_name("INPUT_FILE")
			.help("Specifies the name(s) of the input file or files to convert.")
			.multiple(true)
			.required(true)
			.index(1))
		.get_matches();

	// Construct configuration according to the parsed command line arguments
	let config = Config::new(arg_matches);

	// Run the application an potentially report errors
	process::exit(match nagodigo::run(config)
	{
		Ok(()) => 0,
		Err(error) =>
		{
			eprintln!("Program stopped due to error: {}\n\
				Remove the faulty input file or the -e flag if you want to attempt conversion of the remaining files.", error);
			1
		}
	});
}
