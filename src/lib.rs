/*
 * Nagodigo - PSPICE to SPICE3 / PROSPICE model converter
 * Copyright (c) 2021.  Vladimir Garistov vl.garistov@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#![doc = include_str!("../README.md")]

mod spice_iter;
mod param;
mod components;
mod subckt;
mod component_list;

use crate::component_list::ComponentList;
use clap::ArgMatches;
use std::error::Error;
use std::{fs, fmt};
use std::fmt::{Display, Formatter};

// TODO add more errors
/// Describes the possible errors that can occur during model conversion.
#[derive(Debug)]
pub enum ConversionError
{
	Unsupported(u32),
	ParsingError(u32),
	MissingEndStatement(String),
	SubcircuitOptionalNodes(String),
	SubcircuitText(String),
	DependentSourceType(String),
	SyntaxError(String),
	ParameterNotFound(String),
	// For shit that should not be possible
	UnexpectedError()
}

impl Display for ConversionError
{
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result
	{
		match self
		{
			ConversionError::Unsupported(line) => write!(f, "unsupported component or directive on line {}", line),
			ConversionError::ParsingError(line) => write!(f, "failed to parse line {}", line),
			ConversionError::MissingEndStatement(subcircuit_name) =>
			{
				if subcircuit_name.eq("")
				{
					write!(f, "missing .END statement at the end of the file")
				}
				else
				{
					write!(f, "missing .END statement at the end of subcircuit {}", subcircuit_name)
				}
			},
			ConversionError::SubcircuitOptionalNodes(spice_text) => write!(f, "optional subcircuit nodes are not supported\n\t\"{}\"", spice_text),
			ConversionError::SubcircuitText(spice_text) => write!(f, "subcircuit text parameters are not supported\n\t\"{}\"", spice_text),
			ConversionError::DependentSourceType(spice_text) => write!(f, "unsupported dependant source type\n\t\"{}\"", spice_text),
			ConversionError::SyntaxError(spice_text) => write!(f, "SPICE syntax error\n\t\"{}\"", spice_text),
			ConversionError::ParameterNotFound(par) => write!(f, "Parameter \"{}\" not found", par),
			ConversionError::UnexpectedError() => write!(f, "¯\\_(ツ)_/¯   IDK, contact the developers")
		}
	}
}

impl Error for ConversionError
{}

/// Used to pass down information needed for error reporting. The errors_ignored flag signals that some errors were
/// encountered but ignored because the force flag is up.
pub struct ErrorInfo<'a, 'b>
{
	pub filename: &'a String,
	pub force: bool,
	pub errors_ignored: &'b mut bool
}

impl<'a, 'b> ErrorInfo<'a, 'b>
{
	///	Constructs an instance of [`ErrorInfo`].
	pub fn new(filename: &'a String, force: bool, errors_ignored: &'b mut bool) -> ErrorInfo<'a, 'b>
	{
		ErrorInfo
		{
			filename,
			force,
			errors_ignored
		}
	}
}

/// Contains the runtime configuration of the program. Constructed from the command line.
pub struct Config
{
	pub input_files: Vec<String>,
	pub output_files: Vec<String>,
	pub force: bool,
	pub stop_on_error: bool,
	pub split: bool
}

impl Config
{
	/// Constructs the runtime configuration based on the parsed command line arguments.
	pub fn new(arguments: ArgMatches) -> Config
	{
		Config
		{
			// Unwrap is acceptable here because INPUT_FILE is declared as required
			input_files: arguments.values_of("INPUT_FILE").unwrap().map(|s| s.to_owned()).collect(),
			output_files: match arguments.values_of("output-filename")
			{
				Some(filenames) => filenames.map(|s| s.to_owned()).collect(),
				None => Vec::new(),
			},
			force: arguments.is_present("force"),
			stop_on_error: arguments.is_present("stop_on_error"),
			split: arguments.is_present("split")
		}
	}
}

// TODO: Add a thread pool
/// The de-facto start of application code. Iterates over all files that are being converted.
pub fn run(config: Config) -> Result<(), Box<dyn Error>>
{
	let mut out_files = config.output_files.iter();

	for in_file in config.input_files.iter()
	{
		let res;
		if let Some(out_file) = out_files.next()
		{
			res = convert_file(in_file, out_file, &config);
		}
		else
		{
			let mut out_file = in_file.clone();
			if let Some(dot_index) = in_file.rfind(".")
			{
				out_file.insert_str(dot_index, "-SPICE3");
			}
			else
			{
				out_file.push_str("-SPICE3");
			}
			res = convert_file(in_file, &out_file, &config);
		}
		if let Err(error) = res
		{
			eprintln!("ERROR while converting {}: {}", in_file, error);
			if config.stop_on_error
			{
				return Err(error);
			}
		}
	}
	Ok(())
}

/// Converts a single file with filename in_file and writes the results to a file with filename specified by out_file.
fn convert_file(in_file: &String, out_file: &String, conf: &Config) -> Result<(), Box<dyn Error>>
{
	let mut errors_ignored = false;
	let mut error_info = ErrorInfo::new(in_file, conf.force, &mut errors_ignored);
	println!("Converting '{}' to '{}'...", in_file, out_file);
	let mut netlist = ComponentList::from_file(&mut error_info)?;
	netlist.convert(&mut error_info)?;
	netlist.replace_params()?;
	fs::write(out_file, format!("{}", netlist))?;
	if errors_ignored
	{
		println!("Conversion of '{}' succeeded with errors. Check stderr.", in_file);
	}
	else
	{
		println!("Conversion of '{}' successful.", in_file);
	}
	Ok(())
}