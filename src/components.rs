/*
 * Nagodigo - PSPICE to SPICE3 / PROSPICE model converter
 * Copyright (c) 2021.  Vladimir Garistov vl.garistov@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//! SPICE components/directives and conversions

use crate::param::Param;
use crate::subckt::Subcircuit;
use crate::{ConversionError, ErrorInfo};
use std::fmt::Display;
use std::fmt;
use std::error::Error;
use std::cell::RefCell;
use std::rc::Rc;

/// Declares the methods required for model conversion
#[allow(non_camel_case_types)]
pub trait SPICE_Conversion: Display
{
	/// Performs the necessary conversion for the given component
	fn convert(&mut self, global_subcircuits: &Vec<Rc<RefCell<Subcircuit>>>, error_info: &mut ErrorInfo)
	           -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	/// Replace any referenced parameters. Also evaluates expressions to constants and creates copies of subcircuits
	/// that are referenced with explicit parameter values.
	fn replace_params(&mut self, params: Vec<&Vec<Param>>) -> Result<(), Box<dyn Error>>
	{
		Ok(())
	}
}

/// A single SPICE component or directive
pub enum Component
{
	Resistor(String),
	Capacitor(String),
	Inductor(String),
	MutualInductance(String),
	IndependentSource(String),
	DependentSource(String),
	ControlledSwitch(String),
	Diode(String),
	BJT(String),
	MOSFET(String),
	JFET(String),
	MESFET(String),
	TransmissionLine(String),
	UnsupportedComponent(String),   // IGBT, digital primitives, unsupported or unknown directives
	SubcircuitRef(String),
	SubcircuitDefinition(Rc<RefCell<Subcircuit>>),
	End(String),
	Model(String),
	Option(String),
	Comment(String)
}

impl Component
{
	// TODO: Tests, including for whitespace issues
	/// Parses a SPICE component or directive and constructs a [`Component`]
	///
	/// # Example
	/// ```ignore
	/// let component = Component::from_str("R1 1 0 1k\n").unwrap();
	/// assert_eq!(component, Component::Resistor(String::from("R1 1 0 1k")));
	/// ```
	pub fn from_str(input: &str) -> Option<Component>
	{
		if !input.is_ascii() {return None;}
		let mut input_chars = input.chars();
		if let Some(first_char) = input_chars.next()
		{
			match first_char
			{
				'R' | 'r' => Some(Component::Resistor(String::from(input))),
				'C' | 'c' => Some(Component::Capacitor(String::from(input))),
				'L' | 'l' => Some(Component::Inductor(String::from(input))),
				'K' | 'k' => Some(Component::MutualInductance(String::from(input))),
				'V' | 'v' | 'I' | 'i' => Some(Component::IndependentSource(String::from(input))),
				'G' | 'g' | 'E' | 'e' | 'F' | 'f' | 'H' | 'h' => Some(Component::DependentSource(String::from(input))),
				'S' | 's' | 'W' | 'w' => Some(Component::ControlledSwitch(String::from(input))),
				'D' | 'd' => Some(Component::MESFET(String::from(input))),
				'Q' | 'q' => Some(Component::MESFET(String::from(input))),
				'M' | 'm' => Some(Component::MESFET(String::from(input))),
				'J' | 'j' => Some(Component::MESFET(String::from(input))),
				'B' | 'b' => Some(Component::MESFET(String::from(input))),
				'T' | 't' => Some(Component::MESFET(String::from(input))),
				'X' | 'x' => Some(Component::SubcircuitRef(String::from(input))),
				' ' | '\t' | '*' | ';' => Some(Component::Comment(String::from(input))),
				'.' =>
					if let Some(second_char) = input_chars.next()
					{
						// The first two characters are enough to identify but not to verify a directive. Partial verification is
						// performed during the actual conversion. However, Nagodigo is NOT a complete SPICE parsing/verification software.
						match second_char
						{
							'M' | 'm' => Some(Component::Model(String::from(input))),
							'O' | 'o' | 'T' | 't' => Some(Component::Option(String::from(input))),
							'S' | 's' =>
								if let Some(new_subcircuit) = Subcircuit::from_str(input)
								{
									Some(Component::SubcircuitDefinition(Rc::new(RefCell::new(new_subcircuit))))
								}
								else
								{
									Some(Component::UnsupportedComponent(String::from(input)))
								},
							'E' | 'e' => Some(Component::End(String::from(input))),
							'P' | 'p' => None,
							_ => Some(Component::UnsupportedComponent(String::from(input)))
						}
					}
					else
					{
						Some(Component::UnsupportedComponent(String::from(input)))
					},
				_ => Some(Component::UnsupportedComponent(String::from(input)))
			}
		}
		else
		{
			None
		}
	}
}

impl Display for Component
{
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		match self
		{
			Component::Resistor(spice_text) |
			Component::Capacitor(spice_text) |
			Component::Inductor(spice_text) |
			Component::MutualInductance(spice_text) |
			Component::IndependentSource(spice_text) |
			Component::DependentSource(spice_text) |
			Component::ControlledSwitch(spice_text) |
			Component::Diode(spice_text) |
			Component::BJT(spice_text) |
			Component::MOSFET(spice_text) |
			Component::JFET(spice_text) |
			Component::MESFET(spice_text) |
			Component::TransmissionLine(spice_text) |
			Component::UnsupportedComponent(spice_text) |
			Component::SubcircuitRef(spice_text) |
			Component::End(spice_text) |
			Component::Model(spice_text) |
			Component::Option(spice_text) |
			Component::Comment(spice_text) => write!(f, "{}\n", spice_text),
			Component::SubcircuitDefinition(sub) => write!(f, "{}\n", sub.borrow())
		}
	}
}

impl SPICE_Conversion for Component
{
	// TODO: Change return type to Result<Option<Component>, Box<dyn Error>>
	fn convert(&mut self, global_subcircuits: &Vec<Rc<RefCell<Subcircuit>>>, error_info: &mut ErrorInfo)
	           -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		// TODO: Replace MOSFETs that have unsupported models with subcircuits. Keep a list of MOSFET models similarly to the list of subcircuits.
		// Add those MOSFETs to the renaming requests
		let res = match self
		{
			Component::Resistor(spice_text) => component_conversion::convert_resistor(spice_text),
			Component::Capacitor(spice_text) => component_conversion::convert_capacitor(spice_text),
			Component::Inductor(spice_text) => component_conversion::convert_inductor(spice_text),
			Component::MutualInductance(spice_text) => component_conversion::convert_mutual_inductance(spice_text),
			Component::IndependentSource(spice_text) => component_conversion::convert_independent_source(spice_text),
			Component::DependentSource(spice_text) => component_conversion::convert_dependent_source(spice_text),
			Component::ControlledSwitch(spice_text) => component_conversion::convert_controlled_switch(spice_text),
			Component::Diode(spice_text) => component_conversion::convert_diode(spice_text),
			Component::BJT(spice_text) => component_conversion::convert_BJT(spice_text),
			Component::MOSFET(spice_text) => component_conversion::convert_MOSFET(spice_text),
			Component::JFET(spice_text) => component_conversion::convert_JFET(spice_text),
			Component::MESFET(spice_text) => component_conversion::convert_MESFET(spice_text),
			Component::TransmissionLine(spice_text) => component_conversion::convert_transmission_line(spice_text),
			Component::UnsupportedComponent(_) => Err(Box::new(ConversionError::UnexpectedError()) as Box<dyn Error>),  // Why TF is this needed?
			Component::SubcircuitRef(spice_text) => component_conversion::convert_subcircuit_ref(spice_text),
			Component::End(_) => Ok(None),
			Component::Model(spice_text) => component_conversion::convert_model(spice_text),
			Component::Option(spice_text) => component_conversion::convert_option(spice_text),
			Component::Comment(spice_text) => component_conversion::convert_comment(spice_text),
			Component::SubcircuitDefinition(sub) => sub.borrow_mut().convert(global_subcircuits, error_info)
		};

		if let Err(error) = res
		{
			// If a subcircuit is being converted then error_info.force has already been checked
			if let Component::SubcircuitDefinition(_) = self
			{
				Err(error)
			}
			else if error_info.force
			{
				*error_info.errors_ignored = true;
				eprintln!("ERROR while converting {}: {}", error_info.filename, error);
				Ok(None)
			}
			else
			{
				Err(error)
			}
		}
		else
		{
			res
		}
	}

	// TODO
	fn replace_params(&mut self, params: Vec<&Vec<Param>>) -> Result<(), Box<dyn Error>>
	{
		Ok(())
	}

}

// TODO: a shitload of things
/// Individual conversion functions for the different component types and directives.
/// Some validation is performed to ensure no PSPICE-exclusive features that can't be converted are used but an Ok result from these functions
/// and the ones calling them should not be taken as a guarantee that the input is a valid SPICE netlist.
/// The optional Component return value is not the complete result of the conversion. It is used when the SPICE3 equivalent of a PSPICE
/// component consists of multiple components/directives and contains the additional components and/or directives.
#[allow(non_snake_case)]
mod component_conversion
{
	use super::Component;
	use std::error::Error;
	use crate::ConversionError;
	use std::fmt::Write;

	// TODO: Check corresponding model and multiply value by resistance multiplier. The resulting {expression} will be evaluated by replace_param()
	pub fn convert_resistor(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_capacitor(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_inductor(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_mutual_inductance(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_independent_source(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_dependent_source(component: &mut String)	-> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		let component_uppercase = component.to_uppercase();
		// TODO: LAPlACE and TABLE sources might be convertible. Ask Labcenter Electronics for more documentation.
		if component_uppercase.contains(" LAPLACE ") || component_uppercase.contains(" TABLE ") ||
			component_uppercase.contains(" CHEBYSHEV ") || component_uppercase.contains(" FREQ ")
		{
			return Err(Box::new(ConversionError::DependentSourceType(component.clone())));
		}

		if component_uppercase.contains(" POLY(")
		{
			let component_split = component.split_ascii_whitespace().collect::<Vec<&str>>();
			let l = component_split.len();
			if l < 6 {return Err(Box::new(ConversionError::SyntaxError(component.clone())));}

			let mut buffer = format!("B{} {} {}", component_split[0], component_split[1], component_split[2]);

			// Determine source kind
			let voltage_controlled;
			let voltage_source;
			if component_split[0].starts_with('E') || component_split[0].starts_with('e')
			{
				voltage_controlled = true;
				voltage_source = true;
			}
			else if component_split[0].starts_with('G') || component_split[0].starts_with('g')
			{
				voltage_controlled = true;
				voltage_source = false;
			}
			else if component_split[0].starts_with('H') || component_split[0].starts_with('h')
			{
				voltage_controlled = false;
				voltage_source = true;
			}
			else if component_split[0].starts_with('F') || component_split[0].starts_with('F')
			{
				voltage_controlled = false;
				voltage_source = false;
			}
			else {return Err(Box::new(ConversionError::UnexpectedError()));}
			if voltage_source
			{
				buffer.push_str(" V=");
			}
			else
			{
				buffer.push_str(" I=");
			}

			let start_of_dimensions = component.find('(').unwrap();
			let end_of_dimensions = if let Some(index) = component.find(')')
			{
				index
			}
			else
			{
				return Err(Box::new(ConversionError::SyntaxError(component.clone())));
			};
			let dimensions = component[(start_of_dimensions + 1)..end_of_dimensions].parse::<usize>()?;

			let mut poly_args_iter = component[(end_of_dimensions + 1)..].split_ascii_whitespace();
			let mut controls: Vec<String> = Vec::with_capacity(dimensions);
			let mut controls_found = 0;
			if voltage_controlled
			{
				while let Some(arg1) = poly_args_iter.next()
				{
					if let Some(arg2) = poly_args_iter.next()
					{
						let control_buf = format!("V({},{})", arg1, arg2);
						controls.push(control_buf);
						controls_found += 1;
					}
					if controls_found == dimensions {break;}
				}
			}
			else
			{
				while let Some(arg) = poly_args_iter.next()
				{
					let control_buf = format!("I({})", arg);
					controls.push(control_buf);
					controls_found += 1;
					if controls_found == dimensions {break;}
				}
			}
			if controls_found < dimensions {return Err(Box::new(ConversionError::SyntaxError(component.clone())));}
			let mut polynomial_coefficients = String::new();
			while let Some(coef) = poly_args_iter.next()
			{
				polynomial_coefficients.push_str(coef);
			}
			let expanded_polynomial = expand_polynomial(&controls, &polynomial_coefficients);
			buffer.push_str(&expanded_polynomial);

			*component = buffer.clone();
		}
		else if component_uppercase.contains(" VALUE ") || component_uppercase.contains(" VALUE=")
		{

		}
		// Nothing to do in the linear case

		Ok(None)
	}

	pub fn convert_controlled_switch(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_diode(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_BJT(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_JFET(component: &mut String ) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_MOSFET(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_MESFET(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_transmission_line(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	// Uses Subckt::from_ref() and removes the parameters
	pub fn convert_subcircuit_ref(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_end(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	// TODO: Check if an unsupported parameter is used
	pub fn convert_model(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_option(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		Ok(None)
	}

	pub fn convert_comment(component: &mut String) -> Result<Option<Vec<Component>>, Box<dyn Error>>
	{
		*component = format!("*{}", &component[1..]);
		Ok(None)
	}

	// #################################################################################################################
	// Conversion helper functions
	// TODO: functions for validating component names, nodes and numeric constants?

	/// Expands a POLY statement. Used during the conversion of some dependent sources.
	fn expand_polynomial(controls: &Vec<String>, coef_list: &str) -> String
	{
		let mut p = String::new();
		let n = controls.len();
		if n < 1 {return p;}
		let mut powers: Vec<u16> = vec![0; n];
		let mut coef_iter = coef_list.split_ascii_whitespace();
		if let Some(next_coef) = coef_iter.next()
		{
			p.push_str(next_coef);
		}
		while let Some(next_coef) = coef_iter.next()
		{
			p.push('+');
			increment_poly_powers(&mut powers[..]);
			p.push_str(next_coef);
			for i in 0..n
			{
				if powers[i] != 0
				{
					p.push('*');
					p.push_str(&controls[i]);
					p.push('^');
					p.push_str(&powers[i].to_string());
				}
			}
		}
		p
	}

	/// Procedurally generates the powers corresponding to all variables in an n-dimensional polynomial.
	fn increment_poly_powers(powers: &mut [u16])
	{
		let l = powers.len();
		if l < 1 {return;}
		if l == 1
		{
			powers[0] += 1;
			return;
		}
		let first_zero = powers[0] == 0;
		let middle_zeroes = powers[1..(l - 1)].iter().all(|x| *x == 0);
		// If all but the last (irrespective of the last) powers are 0, then set the first power equal to 1 + the last and the last to 0
		if first_zero && middle_zeroes
		{
			powers[0] = powers[l - 1] + 1;
			powers[l - 1] = 0;
		}
		// If after a given power all others are 0 then decrement this power and increment the next one
		else
		{
			let mut rev_power_iter = powers.iter().rev();
			let mut i = l - 1;
			while let Some(0) = rev_power_iter.next()
			{
				i -= 1;
			}
			// Otherwise decrement the first power (only if it's not zero) and recursively increment the rest of the list
			if i == l - 1
			{
				if middle_zeroes
				{
					powers[0] -= 1;
				}
				increment_poly_powers(&mut powers[1..]);
			}
			else
			{
				powers[i] -= 1;
				powers[i + 1] += 1;
			}
		}
	}

	#[cfg(test)]
	mod tests
	{
		#[test]
		fn poly_4d()
		{
			let mut powers: Vec<u16> = vec![0; 4];
			let mut result: Vec<Vec<u16>> = Vec::with_capacity(26);
			for i in 0..27
			{
				result.push(powers.clone());
				super::increment_poly_powers(&mut powers[..]);
			}
			assert_eq!(result, vec![
				vec![0, 0, 0, 0],
				vec![1, 0, 0, 0],
				vec![0, 1, 0, 0],
				vec![0, 0, 1, 0],
				vec![0, 0, 0, 1],
				vec![2, 0, 0, 0],
				vec![1, 1, 0, 0],
				vec![1, 0, 1, 0],
				vec![1, 0, 0, 1],
				vec![0, 2, 0, 0],
				vec![0, 1, 1, 0],
				vec![0, 1, 0, 1],
				vec![0, 0, 2, 0],
				vec![0, 0, 1, 1],
				vec![0, 0, 0, 2],
				vec![3, 0, 0, 0],
				vec![2, 1, 0, 0],
				vec![2, 0, 1, 0],
				vec![2, 0, 0, 1],
				vec![1, 2, 0, 0],
				vec![1, 1, 1, 0],
				vec![1, 1, 0, 1],
				vec![1, 0, 2, 0],
				vec![1, 0, 1, 1],
				vec![1, 0, 0, 2],
				vec![0, 3, 0, 0],
				vec![0, 2, 1, 0]
			]);
		}

		#[test]
		fn poly_2d()
		{
			let mut powers: Vec<u16> = vec![0; 2];
			let mut result: Vec<Vec<u16>> = Vec::with_capacity(10);
			for i in 0..10
			{
				result.push(powers.clone());
				super::increment_poly_powers(&mut powers[..]);
			}
			assert_eq!(result, vec![
				vec![0, 0],
				vec![1, 0],
				vec![0, 1],
				vec![2, 0],
				vec![1, 1],
				vec![0, 2],
				vec![3, 0],
				vec![2, 1],
				vec![1, 2],
				vec![0, 3]
			]);
		}
	}
}
