/*
 * Nagodigo - PSPICE to SPICE3 / PROSPICE model converter
 * Copyright (c) 2021.  Vladimir Garistov vl.garistov@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//! A list of SPICE components and directives

use crate::param::Param;
use crate::components::{SPICE_Conversion, Component};
use crate::{ConversionError, ErrorInfo};
use std::fmt::Display;
use std::{fmt, fs};
use std::error::Error;
use crate::spice_iter::SPICE_Iter;
use crate::subckt::Subcircuit;
use std::cell::RefCell;
use std::rc::Rc;

// TODO: Add lists of local models. Detect circular AKOs
/// A list of SPICE components and directives
pub struct ComponentList
{
	params: Vec<Param>,
	contents: Vec<Component>,
	global_subcircuits: Vec<Rc<RefCell<Subcircuit>>>
}

// TODO: add method for splitting long lines
impl ComponentList
{
	/// Parses a text file into a [`ComponentList`]. Generates an internal list of all encountered parameters and subcircuits.
	pub fn from_file(error_info: &mut ErrorInfo) -> Result<ComponentList, Box<dyn Error>>
	{
		let mut params: Vec<Param> = Vec::new();
		let mut contents: Vec<Component> = Vec::new();
		let mut global_subcircuits: Vec<Rc<RefCell<Subcircuit>>> = Vec::new();
		let spice_input = fs::read_to_string(error_info.filename)?;
		let mut spice_input = spice_input.spice_lines();

		while let Some(spice_line) = spice_input.next()
		{
			if let Some(new_component) = Component::from_str(&spice_line)
			{
				if let Component::UnsupportedComponent(_) = new_component
				{
					if error_info.force
					{
						*error_info.errors_ignored = true;
						eprintln!("ERROR while parsing {}: {}", error_info.filename, ConversionError::Unsupported(spice_input.get_line_count()));
						continue;
					}
					else
					{
						return Err(Box::new(ConversionError::Unsupported(spice_input.get_line_count())));
					}
				}

				contents.push(match new_component
				{
					Component::SubcircuitDefinition(sub) =>
					{
						global_subcircuits.push(sub.clone());
						sub.borrow_mut().add_from_spice_iter(&mut spice_input, error_info)?;
						Component::SubcircuitDefinition(sub)
					},
					comp=> comp
				});
			}
			else if let Some(mut new_params) = Param::from_str(&spice_line)
			{
				params.append(&mut new_params);
			}
			else if error_info.force
			{
				*error_info.errors_ignored = true;
				eprintln!("ERROR while parsing {}: {}", error_info.filename, ConversionError::ParsingError(spice_input.get_line_count()));
			}
			else
			{
				return Err(Box::new(ConversionError::ParsingError(spice_input.get_line_count())));
			}
		}
		if let Some(Component::End(_)) = contents.iter().last()
		{
			Ok(ComponentList
			{
				params,
				contents,
				global_subcircuits
			})
		}
		else if error_info.force
		{
			*error_info.errors_ignored = true;
			eprintln!("ERROR while parsing {}: {}", error_info.filename, ConversionError::MissingEndStatement(String::from("")));
			Ok(ComponentList
			{
				params,
				contents,
				global_subcircuits
			})
		}
		else
		{
			Err(Box::new(ConversionError::MissingEndStatement(String::from(""))))
		}
	}

	/// Add a new component to the [`ComponentList`]
	fn add_component(&mut self, component: Component)
	{
		self.contents.push(component);
	}

	/// Add a new parameter to the [`ComponentList`]
	fn add_param(&mut self, param: Param)
	{
		self.params.push(param);
	}

	/// Iterate over all components in the list and replace parameters. Also evaluates expressions to constants and creates copies of subcircuits
	/// that are referenced with explicit parameter values.
	pub fn replace_params(&mut self) -> Result<(), Box<dyn Error>>
	{
		for component in self.contents.iter_mut()
		{
			component.replace_params(vec![&self.params])?;
		}
		Ok(())
	}

	/// Iterate over all components in the list and perform the necessary conversions
	pub fn convert(&mut self, error_info: &mut ErrorInfo) -> Result<(), Box<dyn Error>>
	{
		let mut component_buffer:Vec<Component> = Vec::new();
		for component in self.contents.iter_mut()
		{
			if let Some(mut new_component) = component.convert(&self.global_subcircuits, error_info)?
			{
				// TODO: handle the case when new_component is a subcircuit
				component_buffer.append(&mut new_component);
			}
		}
		self.contents.append(&mut component_buffer);
		Ok(())
	}
}

impl Display for ComponentList
{
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		for component in self.contents.iter()
		{
			write!(f, "{}", component)?;
		}
		Ok(())
	}
}