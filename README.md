# Nagodigo
### Work in progress!!! This software is not yet ready for end-users.
Nagodigo is a SPICE model converter. It converts from PSPICE syntax to SPICE3 syntax.
The end goal is to generate PROSPICE-compatible models and hopefully future versions
will have the option to make use of PROSPICE-specific syntax. PROSPICE is a
proprietary SPICE3-based electronic circuit simulator used in Proteus Design Suite
by Labcenter Electronics.

## Limitations
The following components, directives and PSPICE syntax features are not supported.
Support for some of them is planned for future releases but others cannot be
converted at all. Attempting to convert models that make use of them will cause
errors.\
// TODO

## Usage
```ignore
    nagodigo [FLAGS] [OPTIONS] <INPUT_FILE>...

FLAGS:
    -f, --force            Forces conversion for files that contain mistakes and/or unsupported components and
                           directives.
    -h, --help             Prints help information
    -s, --split            Splits long lines before writing them to the output file.
    -e, --stop-on-error    Stops the conversion whenever an error is encountered, even if the remaining files are
                           correctly formatted.
    -V, --version          Prints version information

OPTIONS:
    -o, --output-filename <FILE>...    Specifies the name of the output file to save the conversion result. Can be
                                       specified multiple times when converting multiple files. Output filenames
                                       correspond to input files in the order in which they are specified.

ARGS:
    <INPUT_FILE>...    Specifies the name(s) of the input file or files to convert.
```

## Conversion methods
// TODO